<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-hasher
// Langue: en
// Date: 01-07-2012 21:49:08
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// H
	'hasher_description' => 'The number of documents in the directory IMG/extension/ site can become too important and affect the performance of the server\'s file system. The solution proposed by this plugin is to "hash" directory IMG/.',
	'hasher_slogan' => 'Save documents in <code>IMG/mp3/a/b/c/file.mp3</code>',
);
?>