<?php

namespace Spip\HashDocuments\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers \hasher_adresser_document()
 * @internal
 */
class HasherTest extends TestCase
{
	public static function setUpBeforeClass(): void {
		require_once dirname(__DIR__) . '/hasher_fonctions.php';
	}


	public function dataHasher() {
		return [
			// Expected
			'avec IMG/' => [
				'png/4/9/0/mon_beau_dessin.png',
				'IMG/png/mon_beau_dessin.png',
			],
			'sans IMG/' => [
				'png/4/9/0/mon_beau_dessin.png',
				'png/mon_beau_dessin.png',
			],
			'logo avec IMG/' => [
				'logo/4/9/0/mon_beau_dessin.png',
				'IMG/logo/mon_beau_dessin.png',
			],
			'logo sans IMG/' => [
				'logo/4/9/0/mon_beau_dessin.png',
				'logo/mon_beau_dessin.png',
			],

			// ?
			'logo extension chelou avec IMG/' => [
				'logo/1/c/9/toto.mp3',
				'IMG/logo/toto.mp3',
			],
			'logo extension chelou sans IMG/' => [
				'logo/1/c/9/toto.mp3',
				'logo/toto.mp3',
			],

			// Unexpected
			'non image #1' => [
				false,
				'x',
			],
			'extension incorrecte, avec IMG' => [
				false,
				'IMG/mp3/mon_beau_dessin.png',
			],
			'extension incorrecte, sans IMG' => [
				false,
				'IMG/mp3/mon_beau_dessin.png',
			],
			'déjà encodé, avec IMG/' => [
				false,
				'IMG/png/4/9/0/mon_beau_dessin.png',
			],
			'déjà encodé, sans IMG/' => [
				false,
				'png/4/9/0/mon_beau_dessin.png',
			],
			'déjà encodé, extension incorrecte, avec IMG/' => [
				false,
				'IMG/mp3/4/9/0/mon_beau_dessin.png',
			],
			'déjà encodé, extension incorrecte, sans IMG/' => [
				false,
				'mp3/4/9/0/mon_beau_dessin.png',
			],
		];
	}

	/**
	 * @dataProvider dataHasher
	 */
	public function testHasher($expected, $file) {
		// Given
		$file;
		// When
		$actual = hasher_adresser_document($file, false);
		//Then
		$this->assertEquals($expected, $actual);
	}


	public function dataDehasher() {
		return [
			// Expected
			'avec IMG/' => [
				'png/mon_beau_dessin.png',
				'IMG/png/4/9/0/mon_beau_dessin.png',
			],
			'sans IMG/' => [
				'png/mon_beau_dessin.png',
				'png/4/9/0/mon_beau_dessin.png',
			],
			'logo avec IMG/' => [
				'logo/mon_beau_dessin.png',
				'IMG/logo/4/9/0/mon_beau_dessin.png',
			],
			'logo sans IMG/' => [
				'logo/mon_beau_dessin.png',
				'logo/4/9/0/mon_beau_dessin.png',
			],

			// ?
			'logo extension chelou avec IMG/' => [
				'logo/toto.mp3',
				'IMG/logo/1/c/9/toto.mp3',
			],
			'logo extension chelou sans IMG/' => [
				'logo/toto.mp3',
				'logo/1/c/9/toto.mp3',
			],

			// Unexpected
			'non image #1' => [
				false,
				'x',
			],
			'déjà décodé, avec IMG/' => [
				false,
				'IMG/png/mon_beau_dessin.png',
			],
			'déjà décodé, sans IMG/' => [
				false,
				'png/mon_beau_dessin.png',
			],
			'logo, déjà décodé, avec IMG/' => [
				false,
				'IMG/logo/mon_beau_dessin.png',
			],
			'logo, déjà décodé, sans IMG/' => [
				false,
				'logo/mon_beau_dessin.png',
			],
		];
	}

	/**
	 * @dataProvider dataDehasher
	 */
	public function testDehasher($expected, $file) {
		// Given
		$file;
		// When
		$actual = hasher_adresser_document($file, true);
		//Then
		$this->assertEquals($expected, $actual);
	}
}
